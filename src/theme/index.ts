import baseStyled, { createGlobalStyle, ThemedStyledInterface } from 'styled-components'
import { normalize } from 'styled-normalize'

const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;900&display=swap');
  ${normalize}
  * {
    font-family: 'Montserrat', sans-serif;
  },
`

export const lightTheme = {
  color: {
    background: '#0e0e0e',
    lightShade: '#ebebeb' /* Cararra */,
    lightAccent: '#ffffff' /* Stack */,
    mainBrand: '#d92231' /* LipStick */,
    darkFaded: '#d9d9d9' /* Clay */,
    darkShade: '#252423' /* Shark */,
    darkAccent: '#c34400' /* Burnt */,
    primary: '#d92231' /* Shadow */,
    variant: '#c34400' /* Shadow */,
    alternative: '#f67e00' /* pumpkin */,
    info: '#333333' /* Shark */,
    success: '#5e9850' /* Fruit Salad */,
    warning: '#db8818' /* Zest */,
    danger: '#f44336' /* Pomegranate */,
    white: '#ffffff' /* White */,
    grey: '#737373' /* Grey */,
    pulpe: '#c34400' /* Burnt */,
    orange: '#f67e00' /* pumpkin */,
  },
}

export const darkTheme = {
  color: {
    lightShade: '#242424' /* Cararra */,
    lightAccent: '#212121' /* Stack */,
    mainBrand: '#d92231' /* LipStick */,
    darkFaded: '#ababab' /* Clay */,
    darkShade: '#252423' /* Shark */,
    darkAccent: '#ffffff' /* Burnt */,
    primary: '#d92231' /* Shadow */,
    variant: '#c34400' /* Shadow */,
    alternative: '#f67e00' /* pumpkin */,
    info: '#eeeeee' /* Shark */,
    success: '#5e9850' /* Fruit Salad */,
    warning: '#db8818' /* Zest */,
    danger: '#f44336' /* Pomegranate */,
    white: '#ffffff' /* White */,
    grey: '#737373' /* Grey */,
    pulpe: '#c34400' /* Burnt */,
    orange: '#f67e00' /* pumpkin */,
  },
}

export default GlobalStyle
export type Theme = typeof lightTheme
export const styled = baseStyled as ThemedStyledInterface<Theme>
