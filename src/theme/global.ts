import { FC } from 'react'
import { styled } from './index'

const Container: FC = styled.div`
  padding-top: calc(92px + 40px);
  background-color: ${(props) => props.theme.color.lightShade};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  min-height: calc(100vh - 92px);
`

const Global = {
  Container,
}

export default Global
