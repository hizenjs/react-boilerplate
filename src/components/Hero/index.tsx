import React, { FC } from 'react'
import Logo from '../../assets/logo.png'
import { keyframes } from 'styled-components'
import { styled } from '../../theme'

const breatheAnimation = keyframes`
 0% { opacity: 1 }
 50% { opacity: 0.6 }
 100% { opacity: 1 }
`

const Layout = styled.div`
  background: #010101;
  display: flex;
  height: 100vh;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`
const Image = styled.img`
  background: #010101;
  object-fit: contain;
  width: 64px;
  height: 64px;
  animation-name: ${breatheAnimation};
  animation-duration: 2s;
  animation-iteration-count: infinite;
`

const Title = styled.h1`
  color: #ffffff;
  margin-top: 60px;
  font-weight: 600;
`

const Subtitle = styled.h2`
  color: #ffffff;
  font-weight: 300;
  margin: 0;
`

const Hero: FC = () => (
  <Layout>
    <Image src={Logo} alt="logo" />
    <Title>React Boilerplate</Title>
    <Subtitle>Using Typescript and Styled Components</Subtitle>
  </Layout>
)

export default Hero
