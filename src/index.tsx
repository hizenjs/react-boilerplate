import React from 'react'
import ReactDOM from 'react-dom'
import GlobalStyle from './theme'
import { Hero } from './components'

ReactDOM.render(
  <React.StrictMode>
    <GlobalStyle />
    <Hero />
  </React.StrictMode>,
  document.getElementById('root'),
)
